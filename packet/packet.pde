BufferedReader reader;
String line;
int p_packet,packet,delta_packet;
boolean drawflag=false;
void setup(){
  size(512,512);
  // text.textの中身を読み込む
}
 
void draw(){
  if(frameCount % 60 == 0){
    packet=read_string();
    delta_packet =packet - p_packet;
    println(delta_packet);
    if(delta_packet > 0) drawflag=true;
    else drawflag=false;
    p_packet=packet;
  }
  if(drawflag){
    background(#F5FA05);
    fill(0);
    textAlign(CENTER);
    textSize(30);
    text(delta_packet+"packet",width/2,height/2); 
  }
  else if(frameCount % 300 ==0) background(0);
}
//cut.txtから文字を読み取りintに変換する
int read_string(){
  reader = createReader("cut.txt"); 
  //try catchでエラー処理
  try
  {
    // 一行ずつ読み込む
    line = reader.readLine();
  }
  catch (IOException e)
  {
    return 0;
  }
  if (line == null)
  {
    return 0;
    
  }
  else
  {
    // 一行ずつ表
    return Integer.parseInt(line);
   
  }
  
}
